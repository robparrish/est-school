#!/usr/bin/env python
import math 
import numpy as np
import matplotlib.pyplot as plt

# => Gaussian Basis Function Object <= #

class Gaussian(object):

    def __init__(
        self,
        x=0.0,
        y=0.0,
        z=0.0,
        a=1.0,
        ):

        self.x = x 
        self.y = y 
        self.z = z 
        self.a = a 
        self.N = (math.pi / (2. * self.a))**(-3./4.)

# => Point Charge Object <= #

class Charge(object):
    
    def __init__(
        self,
        x=0.0,
        y=0.0,
        z=0.0,
        Z=1.0,
        ):
        
        self.x = x 
        self.y = y 
        self.z = z 
        self.Z = Z 

# => Functions for integrals <= #

def nuclear(
    mol,
    ):

    E = 0.0
    for i1, Q1 in enumerate(mol):
        for i2, Q2 in enumerate(mol):
            if i2 >= i1: continue
            E += Q1.Z * Q2.Z / math.sqrt((Q1.x - Q2.x)**2 + (Q1.y - Q2.y)**2 + (Q1.z - Q2.z)**2)
    return E

def overlap(
    basis1,
    basis2,
    ):

    S = np.zeros((len(basis1),len(basis2)))
    for k1, bf1 in enumerate(basis1):
        for k2, bf2 in enumerate(basis2):
            a1 = bf1.a
            a2 = bf2.a
            r12_2 = (bf1.x - bf2.x)**2 + (bf1.y - bf2.y)**2 + (bf1.z - bf2.z)**2
            a12 = a1 * a2 / (a1 + a2)
            K = bf1.N * bf2.N * math.exp(- a12 * r12_2)
            S[k1,k2] = K * (math.pi / (a1 + a2))**(3./2.)

    return S
        
def kinetic(
    basis1,
    basis2,
    ):

    T = np.zeros((len(basis1),len(basis2)))
    for k1, bf1 in enumerate(basis1):
        for k2, bf2 in enumerate(basis2):
            a1 = bf1.a
            a2 = bf2.a
            r12_2 = (bf1.x - bf2.x)**2 + (bf1.y - bf2.y)**2 + (bf1.z - bf2.z)**2
            a12 = a1 * a2 / (a1 + a2)
            K = bf1.N * bf2.N * (- 2. * a12**2 * r12_2 + 3. * a12) * math.exp(- a12 * r12_2) * (math.pi / (a1 + a2))**(3./2.)
            T[k1,k2] = K

    return T
        
def potential(
    basis1,
    basis2,
    mol,
    ):

    V = np.zeros((len(basis1),len(basis2)))
    for k1, bf1 in enumerate(basis1):
        for k2, bf2 in enumerate(basis2):
            a1 = bf1.a
            a2 = bf2.a
            r12_2 = (bf1.x - bf2.x)**2 + (bf1.y - bf2.y)**2 + (bf1.z - bf2.z)**2
            a12 = a1 * a2 / (a1 + a2)
            K = bf1.N * bf2.N * math.exp(- a12 * r12_2) * (math.pi / (a1 + a2))**(3./2.)
            xC = (a1 * bf1.x + a2 * bf2.x) / (a1 + a2)
            yC = (a1 * bf1.y + a2 * bf2.y) / (a1 + a2)
            zC = (a1 * bf1.z + a2 * bf2.z) / (a1 + a2)
            for Q1 in mol: 
                rAC = math.sqrt((xC - Q1.x)**2 + (yC - Q1.y)**2 + (zC - Q1.z)**2)
                if rAC == 0.0:
                    V[k1,k2] -= Q1.Z * K * 2. * math.sqrt(a1 + a2) / math.sqrt(math.pi)
                else:
                    V[k1,k2] -= Q1.Z * K * math.erf(math.sqrt(a1 + a2) * rAC) / rAC

    return V

def eri(
    basis1,
    basis2,
    basis3,
    basis4,
    ):
        
    I = np.zeros((len(basis1),len(basis2),len(basis3),len(basis4)))
    for k1, bf1 in enumerate(basis1):
        for k2, bf2 in enumerate(basis2):
            for k3, bf3 in enumerate(basis3):
                for k4, bf4 in enumerate(basis4):
                    # Bra
                    a1 = bf1.a
                    a2 = bf2.a
                    r12_2 = (bf1.x - bf2.x)**2 + (bf1.y - bf2.y)**2 + (bf1.z - bf2.z)**2
                    a12 = a1 * a2 / (a1 + a2)
                    KC = bf1.N * bf2.N * math.exp(- a12 * r12_2) * (math.pi / (a1 + a2))**(3./2.)
                    xC = (a1 * bf1.x + a2 * bf2.x) / (a1 + a2)
                    yC = (a1 * bf1.y + a2 * bf2.y) / (a1 + a2)
                    zC = (a1 * bf1.z + a2 * bf2.z) / (a1 + a2)
                    # Ket
                    a3 = bf3.a
                    a4 = bf4.a
                    r34_2 = (bf3.x - bf4.x)**2 + (bf3.y - bf4.y)**2 + (bf3.z - bf4.z)**2
                    a34 = a3 * a4 / (a3 + a4)
                    KD = bf3.N * bf4.N * math.exp(- a34 * r34_2) * (math.pi / (a3 + a4))**(3./2.)
                    xD = (a3 * bf3.x + a4 * bf4.x) / (a3 + a4)
                    yD = (a3 * bf3.y + a4 * bf4.y) / (a3 + a4)
                    zD = (a3 * bf3.z + a4 * bf4.z) / (a3 + a4)
                    # ERI
                    rCD = math.sqrt((xC - xD)**2 + (yC - yD)**2 + (zC - zD)**2)
                    rho = (a1 + a2) * (a3 + a4) / (a1 + a2 + a3 + a4)
                    if rCD == 0.0:
                        I[k1,k2,k3,k4] = KC * KD * 2.0 * math.sqrt(rho) / math.sqrt(math.pi)
                    else:
                        I[k1,k2,k3,k4] = KC * KD * math.erf(math.sqrt(rho) * rCD) / rCD

    return I
                    
# => H2 Molecule Problem <= # 

class H2(object):
    
    def __init__(
        self,
        R = 1.4,
        ):

        # Input geometry
        self.R = R
    
        # Nuclear repulsion
        self.Enuc = 1. / self.R
    
        # Molecule and STO-1G Basis
        self.molecule = [
            Charge(x=0.0,y=0.0,z=-0.5*self.R,Z=1.0),
            Charge(x=0.0,y=0.0,z=+0.5*self.R,Z=1.0),
            ]
        self.basis = [Gaussian(x=0.0,y=0.0,z=q.z,a=0.435052) for q in self.molecule]

        # AO Basis Integrals
        self.S = overlap(self.basis,self.basis)
        self.T = kinetic(self.basis,self.basis)
        self.V = potential(self.basis,self.basis,self.molecule)
        self.I = eri(self.basis,self.basis,self.basis,self.basis)
        self.H = self.T + self.V

        # Canonical Orthogonalization
        s, U = np.linalg.eigh(self.S)
        X = np.dot(U,np.diag(s**(-1.0/2.0)))
        self.X = X[:,::-1]

        # Orthogonal Basis Integrals
        self.S2 = np.dot(self.X.T,np.dot(self.S,self.X))
        self.T2 = np.dot(self.X.T,np.dot(self.T,self.X))
        self.V2 = np.dot(self.X.T,np.dot(self.V,self.X))
        IT1 = np.einsum('pqrs,sl->pqrl', self.I, self.X)
        IT2 = np.einsum('pqrl,rk->pqkl', IT1, self.X)
        IT3 = np.einsum('pqkl,qj->pjkl', IT2, self.X)
        self.I2 = np.einsum('pjkl,pi->ijkl', IT3, self.X)
        self.H2 = self.T2 + self.V2

        # CI Hamiltonian
        self.He = np.zeros((6,6))
        self.He[0,0] = 2. * self.H2[0,0] + self.I2[0,0,0,0]
        self.He[1,1] = 2. * self.H2[1,1] + self.I2[1,1,1,1]
        self.He[2,2] = self.H2[0,0] + self.H2[1,1] + self.I2[0,0,1,1]
        self.He[3,3] = self.H2[0,0] + self.H2[1,1] + self.I2[0,0,1,1]
        self.He[4,4] = self.H2[0,0] + self.H2[1,1] + self.I2[0,0,1,1] - self.I2[0,1,1,0]
        self.He[5,5] = self.H2[0,0] + self.H2[1,1] + self.I2[0,0,1,1] - self.I2[0,1,1,0]
        self.He[0,1] = self.I2[0,1,0,1]
        self.He[1,0] = self.I2[0,1,0,1]
        self.He[2,3] = -self.I2[0,1,1,0]
        self.He[3,2] = -self.I2[0,1,1,0]
   
        # Diagonalized Hamiltonian
        self.Ee, self.Ce = np.linalg.eigh(self.He)
        self.Et = self.Ee + self.Enuc

def test1(
    Rs = np.linspace(0.50,7.0,100)
    ):
    
    Ets = np.zeros((len(Rs),6))
    E0s = np.zeros((len(Rs),))
    E1s = np.zeros((len(Rs),))
    for Rind, R in enumerate(Rs):
        h2 = H2(R=R)
        Ets[Rind,:] = h2.Et[:]
        E0s[Rind] = h2.He[0,0] + h2.Enuc
        E1s[Rind] = h2.He[1,1] + h2.Enuc

    plt.clf()
    plt.plot(Rs,Ets)
    plt.plot(Rs,E0s)
    #plt.plot(Rs,E1s)
    plt.axis([0.5, 7.0, -1.0, +1.5])
    plt.xlabel('R [a.u.]')
    plt.ylabel('E [a.u.]')
    plt.legend([('State %1d' % x) for x in range(0,6)] + ['HF State'],loc=1)
    plt.savefig('E.pdf')

if __name__ == '__main__':
    
    test1()

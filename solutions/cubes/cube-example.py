import lightspeed as ls
import numpy as np
import psiw

def run_geom():

    """ An example use of Lightspeed's CubicProp object to save
    density, ESP, and HOMO/LUMO orbital cubes for a given RHF solution
    (from psiw.RHF). Also shows how to save a Molden file from the RHF, and how
    to localize orbitals with IBO and save a Molden file.
    """

    # Molecule corresponding to geom.xyz
    mol = ls.Molecule.from_xyz_file('geom.xyz')

    # Compute RHF
    ref = psiw.RHF.build(
        ls.ResourceList.build(1024,1024),
        mol,
        basisname='cc-pvdz',
        )
    ref.compute_energy()

    # => Molden File (Easy!) <= #

    # This uses ls.Molden.save_molden_file, which can handle arbitrary orbitals
    ref.save_molden_file("C.molden")

    # => Localize Orbitals and save Molden File (More-Involved) <= #

    # Build IAOs
    ibo = ls.IBO(options={
        'resources' : ref.resources,
        'molecule' : ref.molecule,
        'basis' : ref.basis,
        'minbasis' : ref.minbasis,
        'Cref' : ref.tensors['Cocc'],
        })
    
    # The occ-occ Fock matrix
    Focc = ls.Tensor.array(np.diag(ref.tensors['eps_occ']))

    # Find localized occupied orbitals
    U, L, F = ibo.localize(ref.tensors['Cocc'],Focc) 

    # Local orbital atomic charges
    # Q = ibo.orbital_atomic_charges(L)
    # print Q

    # Saves the Molden file the long way
    ls.Molden.save_molden_file(
        "L.molden",
        ref.molecule,
        ref.basis,
        L,
        ls.Tensor.array(np.diag(F)),   # Orbital energies
        ls.Tensor.ones((L.shape[1],)), # Orbital occupations
        True) # True if alpha else False

    # => Cube Files (More-Involved) <= #

    # Get the total density matrix (copy to leave ref.tensors alone)
    Dtot = ls.Tensor.array(ref.tensors['D'])
    Dtot.scale(2.0)

    # Get the HOMO/LUMO orbital coefficients
    nocc = ref.sizes['nocc']
    Cgap = ls.Tensor.array(ref.tensors['C'][:,nocc-1:nocc+1])

    # Build a CubicProp helper object
    cube = ls.CubicProp(options={
        'resources' : ref.resources,
        'molecule' : ref.molecule,
        'basis' : ref.basis,
        'pairlist' : ref.pairlist,
        })
    # Save a density cube corresponding to Dtot in rho.cube
    cube.save_density_cube(
        'rho.cube', 
        Dtot,
        )
    # Save orbital cubes corresponding to HOMO and LUMO in psi0.cube and psi1.cube
    cube.save_orbital_cubes(
        'psi',
        Cgap,
        )
    # Save an ESP cube corresponding to Dtot in esp.cube (takes a moment)
    # cube.save_esp_cube(
    #     'esp.cube',
    #     Dtot,
    #     )
    
if __name__ == '__main__':

    run_geom() 

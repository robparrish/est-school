import math 
import numpy as np
import matplotlib.pyplot as plt

# => Gaussian Basis Function Object <= #

class Gaussian(object):

    def __init__(
        self,
        x=0.0,
        y=0.0,
        z=0.0,
        a=1.0,
        ):

        self.x = x 
        self.y = y 
        self.z = z 
        self.a = a 
        self.N = (math.pi / (2. * self.a))**(-3./4.)

# => Point Charge Object <= #

class Charge(object):
    
    def __init__(
        self,
        x=0.0,
        y=0.0,
        z=0.0,
        Z=1.0,
        ):
        
        self.x = x 
        self.y = y 
        self.z = z 
        self.Z = Z 

# => Functions for integrals <= #

def nuclear(
    mol,
    ):

    E = 0.0
    for i1, Q1 in enumerate(mol):
        for i2, Q2 in enumerate(mol):
            if i2 >= i1: continue
            E += Q1.Z * Q2.Z / math.sqrt((Q1.x - Q2.x)**2 + (Q1.y - Q2.y)**2 + (Q1.z - Q2.z)**2)
    return E

def overlap(
    basis1,
    basis2,
    ):

    S = np.zeros((len(basis1),len(basis2)))
    for k1, bf1 in enumerate(basis1):
        for k2, bf2 in enumerate(basis2):
            a1 = bf1.a
            a2 = bf2.a
            r12_2 = (bf1.x - bf2.x)**2 + (bf1.y - bf2.y)**2 + (bf1.z - bf2.z)**2
            a12 = a1 * a2 / (a1 + a2)
            K = bf1.N * bf2.N * math.exp(- a12 * r12_2)
            S[k1,k2] = K * (math.pi / (a1 + a2))**(3./2.)

    return S
        
def kinetic(
    basis1,
    basis2,
    ):

    T = np.zeros((len(basis1),len(basis2)))
    for k1, bf1 in enumerate(basis1):
        for k2, bf2 in enumerate(basis2):
            a1 = bf1.a
            a2 = bf2.a
            r12_2 = (bf1.x - bf2.x)**2 + (bf1.y - bf2.y)**2 + (bf1.z - bf2.z)**2
            a12 = a1 * a2 / (a1 + a2)
            K = bf1.N * bf2.N * (- 2. * a12**2 * r12_2 + 3. * a12) * math.exp(- a12 * r12_2) * (math.pi / (a1 + a2))**(3./2.)
            T[k1,k2] = K

    return T
        
def potential(
    basis1,
    basis2,
    mol,
    ):

    V = np.zeros((len(basis1),len(basis2)))
    for k1, bf1 in enumerate(basis1):
        for k2, bf2 in enumerate(basis2):
            a1 = bf1.a
            a2 = bf2.a
            r12_2 = (bf1.x - bf2.x)**2 + (bf1.y - bf2.y)**2 + (bf1.z - bf2.z)**2
            a12 = a1 * a2 / (a1 + a2)
            K = bf1.N * bf2.N * math.exp(- a12 * r12_2) * (math.pi / (a1 + a2))**(3./2.)
            xC = (a1 * bf1.x + a2 * bf2.x) / (a1 + a2)
            yC = (a1 * bf1.y + a2 * bf2.y) / (a1 + a2)
            zC = (a1 * bf1.z + a2 * bf2.z) / (a1 + a2)
            for Q1 in mol: 
                rAC = math.sqrt((xC - Q1.x)**2 + (yC - Q1.y)**2 + (zC - Q1.z)**2)
                if rAC == 0.0:
                    V[k1,k2] -= Q1.Z * K * 2. * math.sqrt(a1 + a2) / math.sqrt(math.pi)
                else:
                    V[k1,k2] -= Q1.Z * K * math.erf(math.sqrt(a1 + a2) * rAC) / rAC

    return V

# => 1-Electron Schrodinger Equation Object <= #

class Schrodinger(object):

    def __init__(
        self,
        basis,
        molecule,
        ):

        self.basis = basis
        self.molecule = molecule

        # Nuclear repulsion
        self.Enuc = nuclear(self.molecule)
        
        # Integrals
        self.S = overlap(self.basis,self.basis)
        self.T = kinetic(self.basis,self.basis)
        self.V = potential(self.basis,self.basis,self.molecule)

        # Orthogonalization Matrix
        s, U = np.linalg.eigh(self.S)
        #print (min(s) / max(s))
        self.X = np.dot(U,np.dot(np.diag(s**(-1.0/2.0)),U.T))

        # Total Hamiltonian
        self.H = self.T + self.V

        # Transformed Hamiltonian
        H2 = np.dot(self.X.T,np.dot(self.H,self.X))
        e2, U2 = np.linalg.eigh(H2)

        # Orbital Coefficients
        self.C = np.dot(self.X,U2)

        # Orbital energies (including nuclear-nuclear term)
        self.E = e2 + self.Enuc

# => Even-Tempered Basis Sets <= #

def even_tempered(
    alpha = 0.5,
    beta = 1.5,
    n = 9,
    ):

    return alpha * beta**(np.arange(n) - (n-1)/2.)

# => Test Problems <= #

def Hatom(
    exps = even_tempered(),
    ):

    bas = [Gaussian(x=0.,y=0.,z=0.,a=expv) for expv in exps]
    mol = [Charge(x=0.,y=0.,z=0.,Z=1.)]
    psi = Schrodinger(bas,mol)
    return psi

def H2cation(
    R = 1.5,
    exps = even_tempered(),
    ):

    bas = [Gaussian(x=0.,y=0.,z=0.,a=expv) for expv in exps] + [Gaussian(x=0.,y=0.,z=R,a=expv) for expv in exps]
    mol = [Charge(x=0.,y=0.,z=0.,Z=1.)] + [Charge(x=0.,y=0.,z=R,Z=1.)]
    psi = Schrodinger(bas,mol)
    
    #dT = np.einsum('i,ij,j', psi.C[:,0], psi.T, psi.C[:,0]) - \
    #     np.einsum('i,ij,j', psi.C[:,1], psi.T, psi.C[:,1])
    #dV = np.einsum('i,ij,j', psi.C[:,0], psi.V, psi.C[:,0]) - \
    #     np.einsum('i,ij,j', psi.C[:,1], psi.V, psi.C[:,1])
    #print dT, dV

    return psi

# => Homework Problems <= #

def prob1(
    exps = even_tempered(),
    ):

    psi = Hatom(exps)
    print psi.E

def prob2(
    Rs = np.linspace(0.2,10.0,100),
    exps = even_tempered(),
    ):

    E1 = np.zeros_like(Rs)    
    E2 = np.zeros_like(Rs)    

    for Rind, R in enumerate(Rs):
        psi = H2cation(R,exps)
        E1[Rind] = psi.E[0]
        E2[Rind] = psi.E[1]

    plt.clf()
    plt.plot(Rs,E1,Rs,E2)
    plt.axis([0.2, 10., -0.65, +0.25])
    plt.xlabel('$R$ [a.u.]')
    plt.ylabel('$E$ [a.u.]')
    plt.legend(['$E_0$', '$E_1$'], loc=1)
    plt.savefig('E.pdf')

if __name__ == '__main__':
    #prob1(exps=even_tempered(n=109))
    prob2()
        

    

#!/usr/bin/env python
import sys
import collections
import numpy as np
import lightspeed as ls
import rhf

class RCIS(object):

    """ RCIS computes the restricted configuration interaction singles
        excited states from a given RHF reference.

    Class Attributes:
        options (dict) - default options for RCIS objects. Some options are
            required, others can usually be sensibly defaulted.

    """
    
    # => RCIS Default Options <= #
    
    options = {
    
        # > Geometry < #

        'scf' : None,  # RSCF Wavefunction (required)
 
        # > Numerical Thresholds < #

        'thre_dp' : 1.E-6,   # Threshold above which J/K builds will be double precision
        'thre_sp' : 1.E-14,  # Threshold above which J/K builds will be single precision

        # > Davidson Options < #

        'maxiter' : 70,          # Maximum number of Davidson iterations
        'nmax' : 50,             # Davidson maximum subspace size
        'r_convergence' : 1.E-6, # Davidson convergence
        'norm_cutoff' : 1.E-6,   # Davidson norm cutoff
        'use_disk' : False,      # Davidson use disk?

        # > Numbers/Types of States < #
    
        'do_singlet' : True,  # Do singlets or not?
        'nstate_singlet' : 3, # Number of singlet states
        'nguess_singlet' : 3, # Number of singlet guesses

        'do_triplet' : True,  # Do triplets or not?
        'nstate_triplet' : 3, # Number of triplet states
        'nguess_triplet' : 3, # Number of triplet guesses

    }
        
    def __init__(
        self,
        options, # A dictionary of user options
        ):

        """ Initialize an RCIS object 

        Params:
            options (dict) - the non-default options for the RCIS object

        Object Attributes (Many initialized in compute_energy):
            options (dict) - the finalized list of options, including defaults and user overrides
            sizes (dict) -  integral sizes, keys include natom, nao, nocc, nvir, nmin
            scalars (dict) - double scalars, keys include Enuc, Escf
            tensors (dict) - Tensor objects, keys include:
                C, eps, Cocc, eps_occ, Cvir, eps_vir (from RHF)
                dES, dET (excitation energies of singlet/triplet states)
            resources (ResourceList) - the ResourceList object
            molecule (Molecule) - the Molecule
            basis (Basis) - the Basis object defining the orbital AOs
            pairlist (PairList) - the PairList object defining the sieved orbital AO pairs
            CSs/CTs - amplitudes of singlet/triplet states
            DSs/DTs - difference density matrices of singlet/triplet states
        """

        # => Default Options <= #

        self.options = RCIS.options.copy()
    
        # => Override Options <= #

        for key, val in options.iteritems():
            if key not in self.options:
                raise ValueError('Invalid user option: %s' % key)
            self.options[key] = val 

    def compute_energy(self):

        """ Solve the CIS eigenproblem(s) and populate numerous fields in self """

        # => Title Bars <= #

        print '==> RCIS <==\n'

        # => Problem Geometry <= #

        self.scf       = self.options['scf']
        self.resources = self.scf.resources
        self.molecule  = self.scf.molecule
        self.basis     = self.scf.basis
        self.pairlist  = self.scf.pairlist

        # => SCF Quantities <= #

        self.sizes   = { x : self.scf.sizes[x]   for x in ['natom', 'nao', 'nmo', 'nocc', 'nvir']}
        self.scalars = { x : self.scf.scalars[x] for x in ['Enuc', 'Escf'] }
        self.tensors = { x : self.scf.tensors[x] for x in ['Cocc', 'Cvir', 'eps_occ', 'eps_vir', 'C'] }

        # => Singlet States <= #

        if self.options['do_singlet']:
            self.compute_states(
                True,
                self.options['nstate_singlet'],
                self.options['nguess_singlet'],
                )

        # => Triplet States <= #

        if self.options['do_triplet']:
            self.compute_states(
                False,
                self.options['nstate_triplet'],
                self.options['nguess_triplet'],
                )

        # => All Energies <= #

        Es = []
        Es.append((0.0, 'S')) # Reference Energy
        if self.options['do_singlet']:
            for E in np.nditer(self.tensors['dES'].np):
                Es.append((E,'S'))
        if self.options['do_triplet']:
            for E in np.nditer(self.tensors['dET'].np):
                Es.append((E,'T'))
        Es = sorted(Es)
    
        print 'All Energies:\n'
        print '%4s: %24s %14s %14s %4s' % ('I', 'Total E', 'Ex E', 'Ex E (eV)', 'Type')
        E0 = self.scalars['Escf']
        for Eind, Estr in enumerate(Es):
            E = Estr[0]
            print '%4d: %24.16E %14.10f %14.10f %4s' % (Eind, E + self.scalars['Escf'], E, ls.units['ev_per_H'] * (E), Estr[1]) 
        print ''

        # => Trailer Bars <= #

        print '"He\'s on the jazz, man!"'
        print '   --1SG Bosco "BA" Baracus\n'
        print '==> End RCIS <==\n'
        
    def compute_states(
        self,
        singlet = True,
        nstate = 2,
        nguess = 5,
        ):

        """ Solve the RCIS problem for a given spin block.
        
        This is a helper function called by compute_energy to avoid
            duplicate code for singlet/triplet states. Users should
            call compute_energy only.

        Params:
            singlet (bool) - singlet (True) or triplet (False) states
            nstate (int) - number of states to solve for
            nguess (int) - number of Koopman guess vectors to start with
        """

        title = 'Singlet' if singlet else 'Triplet' 
    
        print '=> %s States <=\n' % title
    
        # Fock-Matrix Contribution (Preconditioner)
        F = ls.Tensor((self.sizes['nocc'], self.sizes['nvir']))
        F[...] += np.einsum('i,a->ia',  np.ones(self.sizes['nocc']), self.tensors['eps_vir'])
        F[...] -= np.einsum('i,a->ia',  self.tensors['eps_occ'], np.ones(self.sizes['nvir']))
    
        # Guess
        print 'Guess Details:' 
        print '  Nguess: %11d' % nguess
        Forder = sorted([(x, xind) for xind, x in np.ndenumerate(F)])
        Cs = []
        for k in range(nguess):
            C = ls.Tensor((self.sizes['nocc'], self.sizes['nvir']))
            kind = Forder[k][1]
            print '  %3d: %r' % (k,kind)
            k1 = kind[0]
            k2 = kind[1]
            C[k1,k2] = +1.0
            Cs.append(C)
        print ''

        print 'Convergence:'
        print '  Maxiter: %11d' % self.options['maxiter']
        print ''

        # Davidson
        dav = ls.Davidson(
            nstate,
            self.options['nmax'],
            self.options['r_convergence'],
            self.options['norm_cutoff'],
            )
        print dav

        # => Davidson Iterations <= # 

        print '%s RCIS Iterations:\n' % title
        print '%4s: %11s' % ('Iter', '|R|')
        converged = False
        for iter in xrange(self.options['maxiter']):

            # Sigma Vector Builds
            Ss = [ls.Tensor.clone(C) for C in Cs]
            for C, S in zip(Cs, Ss):
                # One-Particle Term
                S.np[...] *= F 
                # Two-Particle Term
                D = ls.Tensor.chain([self.tensors['Cocc'], C, self.tensors['Cvir']], [False, False, True])
                if singlet:
                    J = ls.IntBox.coulomb(
                        self.resources,
                        ls.Ewald.coulomb(),
                        self.pairlist,
                        self.pairlist,
                        D,
                        self.options['thre_sp'],    
                        self.options['thre_dp'],    
                        )
                    J2 = ls.Tensor.chain([self.tensors['Cocc'], J, self.tensors['Cvir']], [True, False, False])
                K = ls.IntBox.exchange(
                    self.resources,
                    ls.Ewald.coulomb(),
                    self.pairlist,
                    self.pairlist,
                    D,
                    False,
                    self.options['thre_sp'],    
                    self.options['thre_dp'],    
                    )
                K2 = ls.Tensor.chain([self.tensors['Cocc'], K, self.tensors['Cvir']], [True, False, False])
                if singlet:
                    S.np[...] += 2. * J2.np - K2.np
                else:
                    S.np[...] -= K2.np
        
            # Add vectors/diagonalize Davidson subspace
            Rs, Es = dav.add_vectors(Cs,Ss,use_disk=self.options['use_disk'])

            # Print Iteration Trace
            print '%4d: %11.3E' % (iter, dav.max_rnorm)

            # Check for convergence
            if dav.is_converged:
                converged = True
                break

            # Precondition the desired residuals
            for R, E in zip(Rs, Es):
                R.np[...] /= -(F.np[...] - E)
    
            # Get new trial vectors
            Cs = dav.add_preconditioned(Rs,use_disk=self.options['use_disk'])
        
        # Print Convergence
        print ''
        if converged:
            print '%s RCIS Converged\n' % title
        else:
            print '%s RCIS Failed\n' % title

        # Print energies
        key = 'dES' if singlet else 'dET'
        self.tensors[key] = ls.Tensor.array(np.array([x for x in dav.evals]))

        if singlet:
            self.CSs = [ls.Storage.from_storage(C, F) for C in dav.evecs]
            Cs = self.CSs
        else:
            self.CTs = [ls.Storage.from_storage(C, F) for C in dav.evecs]
            Cs = self.CTs
    
        print '%s Energies:\n' % title
        print '%4s: %24s %14s %14s' % ('I', 'Total E', 'Ex E', 'Ex E (eV)')
        E0 = self.tensors[key].np[0]
        for Eind, E in np.ndenumerate(self.tensors[key]):
            print '%4d: %24.16E %14.10f %14.10f' % (Eind[0], E + self.scalars['Escf'], E, ls.units['ev_per_H'] * E)
        print ''

        # Print residuals
        print '%s Residuals:\n' % title
        for rind, r in enumerate(dav.rnorms):
            print '%4d: %11.3E %s' % (rind, r, 'Converged' if r < self.options['r_convergence'] else 'Not Converged')
        print ''

        # Print amplitudes
        print '%s Amplitudes:\n' % title
        for Cind, C in enumerate(Cs):
            print 'State %4d:' % Cind
            Corder = sorted([(-abs(x), xind) for xind, x in np.ndenumerate(C)])
            for k in range(min(5,len(Corder))):
                k1 = Corder[k][1][0]
                k2 = Corder[k][1][1]
                Cval = C.np[k1,k2]
                print '%5d %5d: %14.6E' % (k1, k2, Cval)
            print ''

        # Form density matrices (act basis)
        print '%s OPDMs:\n' % title
        Ds = []
        for Cind, C in enumerate(Cs):
            print 'State %4d' % Cind
            Dvv = ls.Tensor.chain([C,C],[True,False])
            Doo = ls.Tensor.chain([C,C],[False,True]) 
            D = ls.Tensor([self.sizes['nmo']]*2)
            D.np[:self.sizes['nocc'],:self.sizes['nocc']] -= Doo
            D.np[self.sizes['nocc']:,self.sizes['nocc']:] += Dvv
            Ds.append(D)
        print ''
        if singlet:
            self.DSs = Ds
        else:
            self.DTs = Ds
            
        print '=> End %s States <=\n' % title

# => Test Utilities <= #

def run_h2o_sto3g():

    """ Test a water molecule with known geometry/basis set. """

    mol = ls.Molecule.from_xyz_str("""
        O   0.000000000000  -0.143225816552   0.000000000000
        H   1.638036840407   1.136548822547  -0.000000000000
        H  -1.638036840407   1.136548822547  -0.000000000000""",
        name='h2o',
        scale=1.0)

    ref = rhf.RHF.build(
        #ls.ResourceList.build_cpu(),
        ls.ResourceList.build(1024,1024),
        molecule=mol,
        basisname='sto-3g',
        minbasisname='cc-pvdz-minao',
        options = {
            'g_convergence' : 1.E-7,
        })
    ref.compute_energy()
    cis = RCIS({ 'scf' : ref })
    cis.compute_energy()

    # Crawford Data:
    crawford = {
        'Enuc': 8.002367061810450,
        'Escf':  -74.942079928192, 
        'T1':        0.2872554996,  
        'T2':        0.3444249963, 
        'S1':        0.3564617587, 
        'S2':        0.4160717386,
    }
    
    diffs = {
       'Enuc' : cis.scalars['Enuc'] - crawford['Enuc'], 
       'Escf' : cis.scalars['Escf'] - crawford['Escf'], 
       'T1'   : cis.tensors['dET'].np[0] - crawford['T1'],
       'T2'   : cis.tensors['dET'].np[1] - crawford['T2'],
       'S1'   : cis.tensors['dES'].np[0] - crawford['S1'],
       'S2'   : cis.tensors['dES'].np[1] - crawford['S2'],
        }

    print 'Reference Check:'
    for key in ['Enuc', 'Escf', 'T1', 'T2', 'S1', 'S2']:
        print '  %4s: %11.3E %s' % (key, abs(diffs[key]), 'OK' if abs(diffs[key]) < 1.E-8 else 'BAD')
    print ''

    return cis

if __name__ == '__main__':

    cis = run_h2o_sto3g()

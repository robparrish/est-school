import lightspeed as ls
import rhf
import cis
import sys
import time
import os

# Timestamp
start_time = time.time()
print '*** Started at: %s on %s ***' % (
           time.strftime("%a %b %d %H:%M:%S %Z %Y", time.localtime()),
           os.uname()[1],
           )

molname = sys.argv[1]
basisname = sys.argv[2]

resources = ls.ResourceList.build(1024,1024)

molecule = ls.Molecule.from_xyz_file(molname)

ref = rhf.RHF.build(
    resources,
    molecule,
    basisname=basisname)
ref.compute_energy()

cis = cis.RCIS(options={
    'scf' : ref,
    'nstate_singlet' : 1,
    'nstate_triplet' : 1,
    })
cis.compute_energy()

# Timestamp
stop_time = time.time()
print '*** Stopped at: %s on %s ***' % (
           time.strftime("%a %b %d %H:%M:%S %Z %Y", time.localtime()),
           os.uname()[1],
           )
print '*** Runtime: %.3f [s] ***\n' % (
        stop_time - start_time)

import lightspeed as ls
import numpy as np
import collections

class RHF(object):

    """ RHF computes the restricted Hartree Fock wavefunction and gradient thereof.

    Class Attributes:
        options (dict) - default options for RHF objects. Some options are
            required, others can usually be sensibly defaulted.

    """

    options = {
        
        # > Geometry < #

        'resources' : None,  # Resource List (required)
        'molecule'  : None,  # Molecular Geometry (required)
        'basis'     : None,  # Primary Basis (required)
        'minbasis'  : None,  # MinAO Basis (required)

        # > Number of electrons < #

        'nocc' : None,    # Total number of doubly-occupied orbitals (1st priority)
        'charge' : None,  # Total molecular charge (2nd priority)
                          # Look in molecule.charge (3rd priority)

        # > Guess type < #

        'guess'  : 'sad',  # Guess type, one of "sad", "read", or "purify"
        'Cguess' : None,   # Guess orbitals (nao,nocc), required if guess "read" or "purify"
        
        # > Numerical Thresholds < #

        'thre_pq'        : 1.E-14,  # Schwarz-sieve threshold for pq pairs
        'thre_dp'        : 1.E-6,   # Threshold above which J/K builds will be double precision
        'thre_sp'        : 1.E-14,  # Threshold above which J/K builds will be single precision
        'thre_canonical' : 1.0E-6,  # Canonical orthogonalization threshold
        
        # > DIIS Options < #

        'diis_max_vecs' : 6,      # Maximum number of vectors to DIIS with
        'diis_use_disk' : False,  # Should DIIS use disk or not to save memory?
    
        # > Convergence Options < #

        'maxiter' : 50,           # Maximum number of RHF iterations before failure
        'e_convergence' : 1.E-6,  # Maximum allowed energy change at convergence
        'g_convergence' : 1.E-5,  # Maximum allowed element in orbital gradient at convergence 

        # > Print Options < #

        'print_orbitals' : True,  # Should we print the orbital energies?

    }

    def __init__(
        self,
        options,  # A dictionary of user options
        ):
    
        """ Initialize an RHF object 

        Params:
            options (dict) - the non-default options for the RHF object

        Object Attributes (Many initialized in compute_energy):
            options (dict) - the finalized list of options, including defaults and user overrides
            sizes (dict) -  integral sizes, keys include natom, nao, nocc, nvir, nmin
            scalars (dict) - double scalars, keys include Enuc, Escf
            tensors (dict) - Tensor objects, keys include S, T, V, H, J, K, F, C, D, 
                Cocc, Cvir, eps_occ, eps_vir
            resources (ResourceList) - the ResourceList object
            molecule (Molecule) - the Molecule
            basis (Basis) - the Basis object defining the orbital AOs
            minbasis (Basis) -  the Basis object defining the minimal AOs
            pairlist (PairList) - the PairList object defining the sieved orbital AO pairs
            converged (bool) - Did the SCF converge (True) or not (False)?
        """

        # => Default Options <= #

        self.options = RHF.options.copy()
    
        # => Override Options <= #

        for key, val in options.iteritems():
            if key not in self.options:
                raise ValueError('Invalid user option: %s' % key)
            self.options[key] = val 

        # => Useful Registers <= #

        self.sizes   = {}
        self.scalars = {}
        self.tensors = {}

    def compute_energy(self):

        """ Solve the RHF equations and populate numerous fields in self.
        
        Return:
            (float) the SCF energy

        """

        # => Title Bars <= #

        print '==> RHF <==\n'

        # => Problem Geometry <= #

        self.resources = self.options['resources']
        self.molecule  = self.options['molecule']
        self.basis     = self.options['basis']
        self.minbasis  = self.options['minbasis']

        print self.resources
        print self.molecule
        print self.basis
        print self.minbasis

        self.sizes['natom'] = self.molecule.natom
        self.sizes['nao']   = self.basis.nao
        self.sizes['nmin']  = self.minbasis.nao

        # => Nuclear Repulsion <= #

        self.scalars['Enuc'] = self.molecule.nuclear_repulsion_energy()
        print 'Nuclear Repulsion Energy = %24.16E\n' % self.scalars['Enuc']

        # => Integral Thresholds <= #

        print 'Integral Thresholds:'
        print '  Threshold PQ = %11.3E' % self.options['thre_pq']
        print '  Threshold DP = %11.3E' % self.options['thre_dp']
        print '  Threshold SP = %11.3E' % self.options['thre_sp']
        print ''

        # => PairList <= #

        self.pairlist = ls.PairList.build_schwarz(
            self.basis,
            self.basis,
            True,
            self.options['thre_pq'])
        print self.pairlist

        # => One-Electron Integrals <= #

        print 'One-Electron Integrals:\n'
        self.tensors['S'] = ls.IntBox.overlap(
            self.resources,
            self.pairlist)
        self.tensors['T'] = ls.IntBox.kinetic(
            self.resources,
            self.pairlist)
        self.tensors['V'] = ls.IntBox.potential(
            self.resources,
            ls.Ewald.coulomb(),
            self.pairlist,
            self.molecule.xyzZ)
        self.tensors['H'] = ls.Tensor.array(self.tensors['T'].np + self.tensors['V'].np)

        # => Canonical Orthogonalization <= #

        print 'Canonical Orthogonalization:'
        print '  Threshold = %11.3E' % self.options['thre_canonical']
        self.tensors['X'] = ls.Tensor.canonical_orthogonalize(
            self.tensors['S'], 
            self.options['thre_canonical'])
        self.sizes['nmo'] = self.tensors['X'].shape[1]
        print '  Nmo       = %11d' % self.sizes['nmo']
        print '  Ndiscard  = %11d' % (self.sizes['nao'] - self.sizes['nmo'])
        print ''

        # => Orbital Guess <= #

        if self.options['guess'] == 'sad':
            # Use SAD for the orbitals guess
            print 'SAD Guess:\n'
            C0 = ls.SAD.orbitals(
                self.resources, 
                self.molecule,
                self.basis,
                self.minbasis)
        elif self.options['guess'] in ['read', 'purify']:
            # Use the orbitals in Cguess in some way
            if not self.options['Cguess']:
                raise ValueError('Must set Cguess')
            C0 = self.options['Cguess']
            if self.options['guess'] == 'purify':
                # Orthonormalize the orbitals into the current basis (purification)
                print 'Purify Guess:\n'
                M = ls.Tensor.chain([C0,self.tensors['S'],C0],[True,False,False])
                Mm12 = ls.Tensor.power(M,-1.0/2.0)
                C0 = ls.Tensor.chain([C0,Mm12],[False,False])
            else:
                # Use the orbitals as they are
                print 'Read Guess:\n'
        else:
            raise ValueError('Unrecognized guess: ' + self.options['guess'])
        self.tensors['D'] = ls.Tensor.chain([C0,C0],[False,True])

        # => DIIS <= #

        diis = ls.DIIS(
            self.options['diis_max_vecs'],
            )
        print diis
        
        # => Determine number of electrons (and check for integral singlet) <= #

        # Priority 1: use the value of self.options['nocc'] field if not None
        # Priority 2: use the value of self.options['charge'] plus the self.molecule.nuclear_charge field
        # Priority 3: use the val of self.molecule.charge plus the self.molecule.nuclear_charge field
        if self.options['nocc'] is None:
            if self.options['charge'] is None:
                charge = self.molecule.charge
            else:
                charge = self.options['charge']
            nelec = self.molecule.nuclear_charge - charge
            nalpha = 0.5 * nelec
            if nalpha != round(nalpha):
                raise ValueError('Cannot do fractional electrons. Possibly charge/multiplicity are wrong.') 
            self.sizes['nocc'] = int(nalpha)
        else:
            self.sizes['nocc'] = int(self.options['nocc'])

        self.sizes['nvir'] = self.sizes['nmo'] - self.sizes['nocc']
        print 'Orbital Spaces:'
        print '  Nocc = %d' % self.sizes['nocc']
        print '  Nvir = %d' % self.sizes['nvir']
        print '' 

        # ==> RHF Iterations <== #

        print 'Convergence Options:'
        print '  Max Iterations = %11d'   % self.options['maxiter']
        print '  E Convergence  = %11.3E' % self.options['e_convergence']
        print '  G Convergence  = %11.3E' % self.options['g_convergence']
        print ''

        Eold = 0.0
        converged = False
        print 'RHF Iterations:\n'
        print '%4s: %24s %11s %11s' % ('Iter', 'Energy', 'dE', 'dG')
        for iteration in range(self.options['maxiter']):
            
            # => Compute J Matrix <= #

            self.tensors['J'] = ls.IntBox.coulomb(
                self.resources,
                ls.Ewald.coulomb(),
                self.pairlist,
                self.pairlist,
                self.tensors['D'],
                self.options['thre_sp'],    
                self.options['thre_dp'],    
                )

            # => Compute K Matrix <= #

            self.tensors['K'] = ls.IntBox.exchange(
                self.resources,
                ls.Ewald.coulomb(),
                self.pairlist,
                self.pairlist,
                self.tensors['D'],
                True,
                self.options['thre_sp'],    
                self.options['thre_dp'],    
                )

            # => Build Fock Matrix <= #            

            self.tensors['F'] = ls.Tensor.array(self.tensors['H'])
            self.tensors['F'].np[...] += 2. * self.tensors['J'].np
            self.tensors['F'].np[...] -= 1. * self.tensors['K'].np

            # => Compute Energy <= #

            self.scalars['Escf'] = self.scalars['Enuc'] + \
                 2.0 * self.tensors['D'].vector_dot(self.tensors['H']) + \
                 2.0 * self.tensors['D'].vector_dot(self.tensors['J']) + \
                -1.0 * self.tensors['D'].vector_dot(self.tensors['K']) 
            dE = self.scalars['Escf'] - Eold
            Eold = self.scalars['Escf']

            # => Compute Orbital Gradient <= #

            G1 = ls.Tensor.chain([self.tensors[x] for x in ['X', 'S', 'D', 'F', 'X']], [True] + [False]*4)
            G1.antisymmetrize()
            G1[...] *= 2.0
            self.tensors['G'] = G1
            dG = np.max(np.abs(self.tensors['G']))

            # => Print Iteration <= #
            
            print '%4d: %24.16E %11.3E %11.3E' % (iteration, self.scalars['Escf'], dE, dG)
    
            # => Check Convergence <= #
    
            if iteration > 0 and abs(dE) < self.options['e_convergence'] and dG < self.options['g_convergence']:
                converged = True
                break

            # => DIIS Fock Matrix <= #

            if iteration > 0:
                self.tensors['F'] = diis.iterate(self.tensors['F'], self.tensors['G'])

            # => Diagonalize Fock Matrix <= #

            F2 = ls.Tensor.chain([self.tensors[x] for x in ['X', 'F', 'X']],[True,False,False])
            e2, U2 = ls.Tensor.eigh(F2)
            C2 = ls.Tensor.chain([self.tensors['X'], U2],[False,False])
            e2.name = 'eps'
            C2.name = 'C'
            self.tensors['C'] = C2
            self.tensors['eps'] = e2
            
            # => Aufbau Occupy Orbitals <= #

            self.tensors['Cocc'] = ls.Tensor.array(self.tensors['C'][:,:self.sizes['nocc']])

            # => Compute Density Matrix <= #

            self.tensors['D'] = ls.Tensor.chain([self.tensors['Cocc'], self.tensors['Cocc']], [False, True])

        # => Print Convergence <= #
        
        print ''
        if converged:
            print 'RHF Converged\n'
        else:
            print 'RHF Failed\n'
        self.converged = converged
        
        # => Print Final Energy <= #
        
        print 'RHF Energy = %24.16E\n' % self.scalars['Escf']

        # => Cache the Orbitals/Eigenvalues (for later use) <= #

        self.tensors['Cvir'] = ls.Tensor.array(self.tensors['C'][:,self.sizes['nocc']:])
        self.tensors['eps_occ'] = ls.Tensor.array(self.tensors['eps'][:self.sizes['nocc']])
        self.tensors['eps_vir'] = ls.Tensor.array(self.tensors['eps'][self.sizes['nocc']:])

        # => Print Orbital Energies <= #

        if self.options['print_orbitals']:
            print self.tensors['eps_occ']
            print self.tensors['eps_vir']

        # => Trailer Bars <= #

        print '"I love it when a plan comes together!"'
        print '        --LTC John "Hannibal" Smith\n'
        print '==> End RHF <==\n'

        return self.scalars['Escf']

    # Compute the RHF Nuclear Gradient 
    def compute_gradient(
        self,
        ):

        """ Compute the RHF gradient and return the result.
        
        compute_energy must be called first

        Returns:
            (natom,3) Tensor with gradient

        """

        if 'Enuc' not in self.scalars:
            raise ValueError("Must call compute_energy first")

        # => Density Matrices <= #

        # OPDM 
        D = self.tensors['D']
        # Energy-weighted OPDM
        C1 = self.tensors['Cocc']
        C2 = ls.Tensor.zeros_like(C1)
        C2.np[...] = np.einsum('pi,i->pi', C1, self.tensors['eps_occ'])
        W = ls.Tensor.chain([C1,C2],[False,True])

        # => Gradient Contributions <= # 
        
        # We prefer OrderedDict to keep the contributions in order
        grads = collections.OrderedDict()

        # Nuclear repulsion energy gradient
        grads['Nuc'] = self.molecule.nuclear_repulsion_grad()

        # Overlap integral gradient
        grads['S'] = ls.IntBox.overlapGrad(
            self.resources,
            self.pairlist,
            W,
            )
        grads['S'].np[...] *= -2.0

        # Kinetic integral gradient
        grads['T'] = ls.IntBox.kineticGrad(
            self.resources,
            self.pairlist,
            D,
            )
        grads['T'].np[...] *= 2.0
    
        # Potential integral gradient
        grads['V'] = ls.IntBox.potentialGrad(
            self.resources,
            ls.Ewald.coulomb(),
            self.pairlist,
            D,
            self.molecule.xyzZ,
            )
        grads['V'].np[...] *= 2.0

        # Coulomb integral gradient
        grads['J'] = ls.IntBox.coulombGrad(
            self.resources,
            ls.Ewald.coulomb(),
            self.pairlist,
            D,
            D,
            self.options['thre_sp'],
            self.options['thre_dp'],
            )
        grads['J'].np[...] *= 2.0
    
        # Exchange integral gradient
        grads['K'] = ls.IntBox.exchangeGrad(
            self.resources,
            ls.Ewald.coulomb(),
            self.pairlist,
            D,
            D,
            True, 
            True,
            True,
            self.options['thre_sp'],
            self.options['thre_dp'],
            )
        grads['K'].np[...] *= -1.0
        
        # Sum the gradient contributions
        G = ls.Tensor.zeros_like(grads['Nuc'])
        G.name = 'G (RHF)'
        for key, val in grads.iteritems():
            G.np[...] += val

        # print '==> RHF Gradient Contributions <==\n'
        # for key, val in grads.iteritems():
        #     val.name = key
        #     print val
        # print G
    
        return G

    def save_molden_file(
        self,
        filename,
        ):

        """ Write a molden file for the canonical SCF orbitals 

        Params:
            filename (str) - the complete filename to write the molden file to

        """

        occ = ls.Tensor.zeros((self.sizes['nmo'],))
        occ[:self.sizes['nocc']] = 1.0
        ls.Molden.write_molden_file(
            filename,
            self.molecule,
            self.basis,
            self.tensors['C'],  
            self.tensors['eps'],
            occ, 
            True,
            ) 

    def update_xyz(
        self,
        xyz,
        use_guess=False,
        ):

        """ Build an RHF object exactly like this one, except with updated molecular coordinates.

        Params:
            xyz [(natom,3) Tensor] - the new coordinates of the atoms of molecule and associated basis sets
            use_guess (bool) - if False, uses existing guess scheme. 
                if True, uses current geometry orbitals, with the 'purify' guess option.
            
        Returns:
            RHF - a deep copy of self with updated coordinates
            
        """

        options2 = self.options.copy()
        options2['molecule'] = self.molecule.update_xyz(xyz)
        options2['basis'] = self.basis.update_xyz(xyz)
        options2['minbasis'] = self.minbasis.update_xyz(xyz)

        if use_guess:
            options2['guess'] = 'purify'
            options2['Cguess'] = self.tensors['Cocc']

        return RHF(options=options2)

    @staticmethod
    def build(
        resources,
        molecule,
        basis=None,
        minbasis=None,
        basisname='cc-pvdz',
        minbasisname='cc-pvdz-minao',
        options={},
        ):

        """ Build an RHF object using sensible defaults and overrides where appropriate.

        Params:
            resources (ResourceList) - the required ResourceList object
            molecule (Molecule) - the required Molecule object
            basis (Basis) - a Basis object for the orbital AO basis. If this is not specified, a Basis
                object will be constructed from the basisname param.
            minbasis (Basis) - a MinBasis object for the minimal AO basis. If this is not specified, a Basis
                object will be constructed from the minbasisname param.
            basisname (str) - the name of the orbital AO basis to construct for molecule, 
                if basis is None
            minbasisname (str) - the name of the minimal AO basis to construct for molecule, 
                if basis is None
            
        Returns:
            The initialized RHF object - you must call compute_energy.

        """
    
        if basis is None:
            basis = ls.Basis.from_gbs_file(
                molecule,
                basisname)
    
        if minbasis is None:
            minbasis = ls.Basis.from_gbs_file(
                molecule,
                minbasisname)
        
        options.update({
            'resources' : resources,
            'molecule'  : molecule,
            'basis'     : basis,
            'minbasis'  : minbasis,
            })
    
        rhf = RHF(options)
        return rhf

# => Test Utilities <= #

def run_h2o():

    """ Test a water molecule with known geometry/basis set. """

    mol = ls.Molecule.from_xyz_str("""
        O 0.000000000000  0.000000000000 -0.129476890157
        H 0.000000000000 -1.494186750504  1.027446102928
        H 0.000000000000  1.494186750504  1.027446102928""", 
        name='h2o',
        scale=1.0)

    rhf = RHF.build(
        ls.ResourceList.build(1024,1024),
        mol,
        basisname='cc-pvdz',
        minbasisname='cc-pvdz-minao',
        options={
            'g_convergence' : 1.0E-7,
        })
    rhf.compute_energy()
    G = rhf.compute_gradient()

    # Reference Values
    Enuc =  8.8014655299726119E+00
    Escf = -7.6021418445110228E+01
    Gscf = ls.Tensor.array([
        [ -1.7572763786897623E-16, -1.7322461679561943E-15, -5.9585214254555763E-02],
        [ -2.8423764287701580E-16, -4.3046460551412868E-02,  2.9792607130256332E-02],
        [  3.0880669591474855E-16,  4.3046460551377508E-02,  2.9792607130674276E-02],
        ])

    # Validity check
    OK = True
    dEnuc = rhf.scalars['Enuc'] - rhf.scalars['Enuc']
    OEnuc = abs(dEnuc) < 1.0E-10 
    OK &= OEnuc
    dEscf = rhf.scalars['Escf'] - rhf.scalars['Escf']
    OEscf = abs(dEscf) < 1.0E-10 
    OK &= OEscf
    dGscf = np.max(np.abs(Gscf[...] - G[...]))
    OGscf = abs(dGscf) < 1.0E-7
    OK &= OGscf
    
    print 'Deviation in Enuc: %11.3E %s' % (dEnuc, 'OK' if OEnuc else 'BAD')
    print 'Deviation in Escf: %11.3E %s' % (dEscf, 'OK' if OEscf else 'BAD')
    print 'Deviation in Gscf: %11.3E %s' % (dGscf, 'OK' if OGscf else 'BAD')
    print 'OK' if OK else 'BAD'

if __name__ == '__main__':
    run_h2o()

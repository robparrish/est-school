import lightspeed as ls
import numpy as np

# => Load Molecule <= #

# Load Molecule from XYZ string representation
mol = ls.Molecule.from_xyz_str("""
    3
    0 1
    O          0.000000000000     0.000000000000    -0.068516219310 
    H          0.000000000000    -0.790689573744     0.543701060724 
    H          0.000000000000     0.790689573744     0.543701060724
    """,
    name='h2o',
    )
print mol

# => Load Basis <= #

basis = ls.Basis.from_gbs_file(mol, 'cc-pvdz')
print basis

# => Other Examples <= #
    
# Force Cartesian angular momentum
# basis = ls.Basis.from_gbs_file(mol, 'cc-pvdz', spherical=False)

# Other common basis sets
# basis = ls.Basis.from_gbs_file(mol, 'sto-3g')
# basis = ls.Basis.from_gbs_file(mol, '6-31g')
# basis = ls.Basis.from_gbs_file(mol, '6-31gs')
# basis = ls.Basis.from_gbs_file(mol, '6-31gss')

# The universal MinAO basis set
# basis = ls.Basis.from_gbs_file(mol, 'cc-pvdz-minao')

# => Attributes <= #

print 'Attributes:\n'

print '%-20s: %r' % ('name', basis.name)
print '%-20s: %r' % ('nao', basis.nao)
print '%-20s: %r' % ('ncart', basis.ncart)
print '%-20s: %r' % ('nprim', basis.nprim)
print '%-20s: %r' % ('nshell', basis.nshell)
print '%-20s: %r' % ('natom', basis.natom)
print '%-20s: %r' % ('has_pure', basis.has_pure)
print '%-20s: %r' % ('max_L', basis.max_L)
print ''

# => Primitives/Shells (Advanced Users Only) <= #

# Primitives/Shells are two different views into the core details of the
# Gaussians in Basis:
#   Primitives gives an uncontracted view
#   Shells gives a contracted view
# The Primitive/Shell classes are simple struct-like classes that contain the
# attributes indicated by their print function

# This prints quite a lot
# print 'Primitives:\n'
# for P, prim in enumerate(basis.primitives):
#     print prim
# print ''

# This prints quite a lot
# print 'Shells:\n'
# for P, shell in enumerate(basis.shells):
#     print shell
# print ''

# => Coordinates <= #

print 'Coordinates:\n'

xyz = basis.xyz
print xyz

# => Update Coordinates <= #

print 'Update Coordinates:\n'
xyz[...] += 4.0  # Adds 4.0 to all xyz coordinates
basis2 = basis.update_xyz(xyz)
print basis2.xyz

# => Concatenate <= #

print 'Concatenate:\n'
basis3 = ls.Basis.concatenate([basis,basis2])
print basis3

# => Subset <= #

print 'Subset:\n'
basis4 = basis3.subset([0,1,2])
print basis4

# => Equivalence <= #

print 'Equivalence: %r\n' % (ls.Basis.equivalent(basis,basis4))

# print dir(basis)

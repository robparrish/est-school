print 'Prepare to make the jump to lightspeed (LS)!'

import lightspeed as ls
import numpy as np

# => Create a Tensor of shape (2,3,4) with all zeros <= #

print 'Tensor Constructor:\n'
T = ls.Tensor((2,3,4))
print T

# => Same, but name the Tensor T2 <= #

T = ls.Tensor((2,3,4),"T2")
print T
# Or you could say
T.name = "T2"

# => Attributes <= #

print 'Attributes:\n'

print '%-20s: %r' % ('name', T.name)
print '%-20s: %r' % ('ndim', T.ndim)
print '%-20s: %r' % ('size', T.size)
print '%-20s: %r' % ('shape', tuple(T.shape))
print '%-20s: %r' % ('strides', tuple(T.strides))
print ''

# => Set the (0,2,1)-th element to 1.0 <= #

T[0,2,1] = 1.0
print T

# => Zero the Tensor <= #

T.zero() 
print T

# => Tensors play nice with numpy <= #

T[...] = np.random.rand(*T.shape)
print T

# => Tensors appear as array-like types <= #

D = np.einsum('ijk,ijk->', T, T)
print D

# => If you want to use numpy operations, just get an ndarray view with the .np attribute <= #

E = ls.Tensor.zeros_like(T)
E[...] = T.np[...] + T.np[...]
print E

# NOTE: Tensor defines an array interface, so anything you can do to an array,
# you can do to a Tensor. Beyond that, the 'np' property of each Tensor returns
# a numpy ndarray view of the tensor. So anything you can do to a numpy
# ndarray, you can do to a Tensor *after* calling '.np'

# => If generating a new Tensor, use the ls.Tensor.array() function <= #

# ls.Tensor.array is important, as it makes a deep copy of any array-type and returns Tensor
F = ls.Tensor.array(T.np[...] + T.np[...])
print F

# => Tensors have some specialized/optimized BLAS-type functions <= #

# Peform the dot product of T with itself
D = T.vector_dot(T) 
# Scale T by 2.0
T.scale(2.0)
# Chain multiply a list of 2D tensors, with a list of transpose arguments
A1 = ls.Tensor((4,5))
A2 = ls.Tensor((5,5))
A3 = ls.Tensor((3,5))
C = ls.Tensor.chain([A1,A2,A3],[False,False,True])
# Permute one tensor into another
C = ls.Tensor.permute('ijk->kij', T)
# Einsum two tensors into another
C = ls.Tensor.einsum('ijk,jl->ikl', T, A3)

# => Tensors also have some useful linear algebra functions <= #

# SPD problem
H = ls.Tensor((5,5))
H[...] = np.random.rand(*H.shape)
H.symmetrize() # Averages off-diagonal elements
S = ls.Tensor.chain([H]*2,[False]*2) # Random SPD tensor
print S

# Eigendecomposition
s, U = ls.Tensor.eigh(S)
print s
print U
    
# Canonical orthogonalization (rows - old basis, cols - orthogonal basis)
# The second argument is the inverse relative condition number
X = ls.Tensor.canonical_orthogonalize(S,1.E-10)
print X

X = ls.Tensor.cholesky_orthogonalize(S)
print X

X = ls.Tensor.lowdin_orthogonalize(S)
print X

print "Traveling through hyperspace ain't like dusting crops, boy!"

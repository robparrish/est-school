import lightspeed as ls
import numpy as np

# => State/Error Vectors <= #

F = ls.Tensor((5,5)) # State vector
G = ls.Tensor((4,4)) # Error vector
F[...] = np.random.rand(*F.shape)
G[...] = np.random.rand(*G.shape)

# => DIIS Tensor interface <= #    

# Build a DIIS object with up to 6 history vectors
diis = ls.DIIS(6) 
print diis

# Iterate the DIIS object
F2 = diis.iterate(F,G)
print type(F2)

# => DIIS Vector of Tensors <= #

# Build a DIIS object with up to 6 history vectors
diis = ls.DIIS(6) 
print diis

# Iterate the DIIS object
F2 = diis.iterate([F],[G])
print type(F2)

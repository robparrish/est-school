import lightspeed as ls
import numpy as np

# => Davidson Object <= #

dav = ls.Davidson(
    2,      # Number of states
    10,     # Number of states before subspace collapse
    1.0E-6, # 2-norm of residual at convergence
    1.0E-6, # Norm cutoff in S in subspace generalized eigenproblem
    )
print dav

# => A trial matrix <= #

N = 10
A = ls.Tensor.zeros((N,N))
A.identity()
A[...] += 0.2 * np.random.rand(N,N)
A.symmetrize()

print A

# => Abar preconditioner matrix <= #

Abar = ls.Tensor.zeros((N,N))
Abar.identity()

# => Generate a guess vector (or more than one) <= #

G = 2
bs = []
for k in range(G):
    b0 = ls.Tensor.zeros((N,))
    b0[k] = 1.0
    bs.append(b0)

# ==> Master Loop <== #

for iter in range(50):

    # Matrix vector products
    ss = [ls.Tensor.einsum('ij,j->i', A, b) for b in bs]

    # Add vectors/diagonalize Davidson subspace
    rs, lambdas = dav.add_vectors(bs,ss) 

    # Print Iteration Trace
    print '%4d: %11.3E' % (iter, dav.max_rnorm)

    # Check for convergence
    if dav.is_converged:
        converged = True
        break

    # Precondition the desired residuals
    for r, l in zip(rs, lambdas):
        r[...] /= -(1.0 - l)

    # Get new trial vectors
    bs = dav.add_preconditioned(rs)

# Print Convergence
print ''
if converged:
    print '%s Dav Converged\n'
else:
    print '%s Dav Failed\n' 

lambdas = list(dav.evals)

xs = [ls.Storage.from_storage(x, bs[0]) for x in dav.evecs]

for x, l in zip(xs, lambdas):
    print l
    print x
    

l2, U2 = ls.Tensor.eigh(A)  
print l2

import lightspeed as ls
import numpy as np

# => Load Molecule <= #

# Note: XYZ files are specified in Angstrom, but LS works in bohr.
# Therefore, the from_xyz_str/from_xyz_file methods have a kwarg called 'scale'
# which is defaulted to ls.units['bohr_per_ang']. You can change this to
# 'scale=1.0' to read in an XYZ file that was written in bohr. The same goes
# for save_xyz_file, except here scale is defaulted to ls.units['ang_per_bohr'].

# Load Molecule from XYZ string representation
mol1 = ls.Molecule.from_xyz_str("""
    3
    0 1
    O          0.000000000000     0.000000000000    -0.068516219310 
    H          0.000000000000    -0.790689573744     0.543701060724 
    H          0.000000000000     0.790689573744     0.543701060724
    """,
    name='h2o',
    )
# Load the same Molecule from XYZ file
mol2 = ls.Molecule.from_xyz_file('h2o.xyz')

# => Save Molecule <= #

# Saves mol1 in 'h2o_v2.xyz' in Angstrom
mol1.save_xyz_file('h2o_v2.xyz')

# => Print Molecule <= #

print 'Printing:\n'

# Short version
print mol1
# Long version (includes coordinates)
print mol1.string(True)

# => Attributes <= #

print 'Attributes:\n'

print '%-20s: %r' % ('name', mol1.name)
print '%-20s: %r' % ('natom', mol1.natom)
print '%-20s: %r' % ('charge', mol1.charge)
print '%-20s: %r' % ('multiplicity', mol1.multiplicity)
print ''

# => Atoms <= #

print 'Atoms:\n' 

for A, atom in enumerate(mol1.atoms):
    print atom
print ''

# => Close Look at mol1.atoms[0] (O) <= #

print 'AtomO:\n'
atomO = mol1.atoms[0]
print '%-20s: %r' % ('label', atomO.label)
print '%-20s: %r' % ('symbol', atomO.symbol)
print '%-20s: %r' % ('N', atomO.N)
print '%-20s: %r' % ('Z', atomO.Z)
print '%-20s: %r' % ('x', atomO.x)
print '%-20s: %r' % ('y', atomO.y)
print '%-20s: %r' % ('z', atomO.z)
print '%-20s: %r' % ('atomIdx', atomO.atomIdx)
print ''

# => Coordinates/Charges <= #

print 'Coordinates/Charges:\n'

xyz = mol1.xyz
print xyz
xyzZ = mol1.xyzZ
print xyzZ
Z = mol1.Z
print Z

# => Update Coordinates <= #

print 'Update Coordinates:\n'
xyz[...] += 4.0  # Adds 4.0 to all xyz coordinates
mol3 = mol1.update_xyz(xyz)
print mol3.xyz

# => Concatenate <= #

print 'Concatenate:\n'
mol4 = ls.Molecule.concatenate([mol1,mol3],0,1)
print mol4

# => Subset <= #

print 'Subset:\n'
mol5 = mol4.subset([0,1,2],0,1)
print mol5

# => Equivalence <= #

print 'Equivalence: %r\n' % (ls.Molecule.equivalent(mol1,mol5))

# => Nuclear Repulsion Energy <= #

print 'Enuc: %24.16E\n' % mol1.nuclear_repulsion_energy()

# => Nuclear Repulsion Energy Gradient <= #

print mol1.nuclear_repulsion_grad()

# => Nuclear Charge/COM/I Tensor <= #    

print 'Nuclear Charge: %r\n' % mol1.nuclear_charge
print mol1.nuclear_COM
print mol1.nuclear_I

# print dir(mol1)


import lightspeed as ls
import numpy as np

# => Build CPU ResourceList <= #

resources = ls.ResourceList.build_cpu()
print resources

# => Load Molecule <= #

# Load Molecule from XYZ string representation
mol = ls.Molecule.from_xyz_str("""
    3
    0 1
    O          0.000000000000     0.000000000000    -0.068516219310 
    H          0.000000000000    -0.790689573744     0.543701060724 
    H          0.000000000000     0.790689573744     0.543701060724
    """,
    name='h2o',
    )
print mol

# => Load Orbital Basis <= #

basis = ls.Basis.from_gbs_file(mol, 'cc-pvdz')
print basis

# => Load MinAO Basis <= #

minbasis = ls.Basis.from_gbs_file(mol, 'cc-pvdz-minao')
print minbasis

# => Compute SAD fractionally-occupied atomic orbitals <= #

Csad = ls.SAD.orbitals(
    resources,
    mol,
    basis,
    minbasis)
print Csad

# => Compute the SAD OPDM (atom block-diagonal) <= #

Dsad = ls.Tensor.chain([Csad,Csad],[False,True])
print Dsad

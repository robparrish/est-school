import lightspeed as ls
import numpy as np

# => Build GPU ResourceList <= #

resources = ls.ResourceList.build()
print resources

# => Load Molecule <= #

# Load Molecule from XYZ string representation
mol = ls.Molecule.from_xyz_str("""
    3
    0 1
    O          0.000000000000     0.000000000000    -0.068516219310 
    H          0.000000000000    -0.790689573744     0.543701060724 
    H          0.000000000000     0.790689573744     0.543701060724
    """,
    name='h2o',
    )
print mol

# => Load Orbital Basis <= #

basis = ls.Basis.from_gbs_file(mol, 'sto-3g')
print basis

# => Load MinAO Basis <= #

minbasis = ls.Basis.from_gbs_file(mol, 'cc-pvdz-minao')
print minbasis

# => Compute SAD OPDM to use in Coulomb/Exchange builds <= #

Csad = ls.SAD.orbitals(
    resources,
    mol,
    basis,
    minbasis)
Dsad = ls.Tensor.chain([Csad,Csad],[False,True])
# print Csad
# print Dsad

# => Build PairList <= #

pairlist = ls.PairList.build_schwarz(
    basis,    # The first basis set
    basis,    # The second basis set
    True,     # The PairList is symmetric in basis1 and basis2
    1.0E-14,  # The Schwarz sieve cutoff
    )
print pairlist

# ==> IntBox <== #

# => Integrals Needed for RHF/RCIS <= #

# Overlap integrals
S = ls.IntBox.overlap(
    resources, # The resources to use
    pairlist,  # The pairlist for the (pq| space
    )
S.name = 'S'
print S

# Kinetic energy integrals
T = ls.IntBox.kinetic(
    resources, # The resources to use
    pairlist,  # The pairlist for the (pq| space
    )
T.name = 'T'
print T

# Nuclear potential integrals
V = ls.IntBox.potential(
    resources,          # The resources to use
    ls.Ewald.coulomb(), # Use the conventional 1/r operator
    pairlist,           # The pairlist for the (pq| space
    mol.xyzZ,           # The positions/magnitudes of the point charges
    )
V.name = 'V'
print V
    
# Coulomb integrals
J = ls.IntBox.coulomb(
    resources,          # The resources to use
    ls.Ewald.coulomb(), # Use the conventional 1/r operator
    pairlist,           # The pairlist for the (pq| space
    pairlist,           # The pairlist for the |rs) space
    Dsad,               # The input OPDM D_rs
    1.0E-14,            # Threshold above which integrals will be done in single precision
    1.0E-6,             # Threshold above which integrals will be done in double precision
    )
J.name = 'J'
print J
    
# Exchange integrals
K = ls.IntBox.exchange(
    resources,          # The resources to use
    ls.Ewald.coulomb(), # Use the conventional 1/r operator
    pairlist,           # The pairlist for the (pq| space
    pairlist,           # The pairlist for the |rs) space
    Dsad,               # The input OPDM D_qs
    True,               # The input OPDM is symmetric (gives 2x speedup)
    1.0E-14,            # Threshold above which integrals will be done in single precision
    1.0E-6,             # Threshold above which integrals will be done in double precision
    )
K.name = 'K'
print K

# => Integrals Needed for RHF/RCIS gradients <= #

# Overlap integral gradient
GS = ls.IntBox.overlapGrad(
    resources, # The resources to use
    pairlist,  # The pairlist for the (pq| space
    Dsad,      # The energy-weighted OPDM W_pq
    )
GS.name = 'GS'
print GS

# Kinetic integral gradient
GT = ls.IntBox.kineticGrad(
    resources, # The resources to use
    pairlist,  # The pairlist for the (pq| space
    Dsad,      # The OPDM D_pq
    )
GT.name = 'GT'
print GT

# Potential integral gradient
GV = ls.IntBox.potentialGrad(
    resources,          # The resources to use
    ls.Ewald.coulomb(), # Use the conventional 1/r operator
    pairlist,           # The pairlist for the (pq| space
    Dsad,               # The OPDM D_pq
    mol.xyzZ,           # The positions/magnitudes of the point charges
    )
GV.name = 'GV'
print GV

# Coulomb integral gradient
GJ = ls.IntBox.coulombGrad(
    resources,          # The resources to use
    ls.Ewald.coulomb(), # Use the conventional 1/r operator
    pairlist,           # The pairlist for the (pq| space and the |rs) space
    Dsad,               # The OPDM D_pq
    Dsad,               # The OPDM D_rs
    1.0E-14,            # Threshold above which integrals will be done in single precision
    1.0E-6,             # Threshold above which integrals will be done in double precision
    )
GJ.name = 'GJ'
print GJ

# Exchange integral gradient
GK = ls.IntBox.exchangeGrad(
    resources,          # The resources to use
    ls.Ewald.coulomb(), # Use the conventional 1/r operator
    pairlist,           # The pairlist for the (pq| space and the |rs) space
    Dsad,               # The OPDM D_pr
    Dsad,               # The OPDM D_qs
    True,               # Is D_pr symmetric?
    True,               # Is D_qs symmetric?
    True,               # Does D_pr == D_qs?
    1.0E-14,            # Threshold above which integrals will be done in single precision
    1.0E-6,             # Threshold above which integrals will be done in double precision
    )
GK.name = 'GK'
print GK

# => Interesting Property Integrals <= #

# Dipole Integrals (needed for permanent and transition dipole moments)
X = ls.IntBox.dipole(
    resources, # The resources to use
    pairlist,  # The pairlist for the (pq| space
    0.0,       # The x origin
    0.0,       # The y origin
    0.0,       # The z origin
    )
X[0].name = 'X'
X[1].name = 'Y'
X[2].name = 'Z'
print X[0]
print X[1]
print X[2]

# Quadrupole Integrals (needed for orbital extents)
Q = ls.IntBox.quadrupole(
    resources, # The resources to use
    pairlist,  # The pairlist for the (pq| space
    0.0,       # The x origin
    0.0,       # The y origin
    0.0,       # The z origin
    )
Q[0].name = 'XX'
Q[1].name = 'XY'
Q[2].name = 'XZ'
Q[3].name = 'YY'
Q[4].name = 'YZ'
Q[5].name = 'ZZ'
print Q[0]
print Q[1]
print Q[2]
print Q[3]
print Q[4]
print Q[5]

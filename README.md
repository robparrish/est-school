# Stanford/PULSE Electronic Structure Summer School #

For most queries (logistical documents, lecture notes, and tutorial exercises), see the Wiki [here](https://bitbucket.org/robparrish/est-school/wiki/Home).

This repository proper will hold python code snippets providing the solutions to tutorial exercises. 

This is a git-based code repository - everything on it is subject to updating. Make sure you are always referring to the tip of the repository, or else always use the SHA for a given commit in communications about the repository contents.

# Funding

Funding for the summer school is provided by the PULSE Institute.

# Contact

Questions about the repository or summer school should be directed to Rob Parrish (rmp89@stanford.edu) or Todd Martinez (Todd.Martinez@stanford.edu).